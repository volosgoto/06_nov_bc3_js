_.every(
	[
		{ name: 'Sara', isActive: true },
		{ name: 'Vova', isActive: true },
	],
	function (item) {
		return item.isActive == true;
	}
);

_.every(
	[
		{ name: 'Sara', isActive: true },
		{ name: 'Vova', isActive: true },
	],
	{ isActive: true }
);
// =========================  reduce ===========

_.reduce(
	[
		{ id: 1, name: 'foo' },
		{ id: 2, name: 'bar' },
	],
	function (memo, item) {
		if (item.name === 'foo') {
			memo.push(item.id);
		}
		return memo;
	},
	[]
);
