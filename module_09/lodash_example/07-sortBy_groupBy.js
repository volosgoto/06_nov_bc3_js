_.sortBy(
	[
		{ name: 'Vova', likes: 15 },
		{ name: 'Sara', likes: 5 },
		{ name: 'John', likes: 1 },
		{ name: 'Bob', likes: 20 },
	],
	function (item) {
		return item.likes;
	}
);

// короткая запись
_.sortBy(
	[
		{ name: 'Vova', likes: 15 },
		{ name: 'Sara', likes: 5 },
		{ name: 'John', likes: 1 },
		{ name: 'Bob', likes: 20 },
	],
	'likes'
);
// ================ groupBy =====================

_.groupBy(
	[
		{ name: 'Vova', isActive: true },
		{ name: 'Sara', isActive: true },
		{ name: 'Mike', isActive: false },
	],
	function (item) {
		return item.isActive;
	}
);

// короткая запись
_.groupBy(
	[
		{ name: 'Vova', isActive: true },
		{ name: 'Sara', isActive: true },
		{ name: 'Mike', isActive: false },
	],
	'isActive'
);
