// let arr = [1, 2, 3].map(function (item) {
// 	return item;
// });

// console.log(arr);

// let newArr = _.map([{ id: 1 }, { id: 2 }, { id: 3 }], function (item) {
// 	return item.id;
// });

// console.log(newArr);

// Забираем ай ди или ключи
let newArr = _.map(
	[
		{ id: 1, name: ' Vova' },
		{ id: 2, name: 'Sara' },
	],
	'name'
);
console.log(newArr); // id

newArr = _.map(
	[
		{ id: 1, name: ' Vova' },
		{ id: 2, name: 'Sara' },
	],
	'id'
);
console.log(newArr); // names
