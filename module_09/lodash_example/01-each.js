// const path = require('path');
// let _ = require('lodash');

// let res = _.isEqual(2, 3);
// _.isArray(['a', 'b']);

// [1, 2].forEach(function (item) {
// 	console.log(item);
// });
// console.log(res);

// ['a', 'b'].forEach(function (item) {
// 	console.log(item);
// });
// _.isArray(['a', 'b']);

// _.each([1, 2], function (item) {
// 	console.log(item);
// });

let user = { name: 'Vova', age: 30 };
// _.each(user, function (item) {
// 	// значения свойств объекта
// 	console.log(item);
// });

_.each(user, function (item, index) {
	console.log(item, index); // свойства и ключи
});

// Пушим с-ва в массив
let arr = [];
_.each(user, function (item) {
	arr.push(item);
});
console.log(arr);
