// _.filter([1, 2, 3, 4, 5], function (item) {
// 	return item < 3;
// });

// let user = { id: 1, name: 'Vova', status: 'admin' };
// let userFilter = _.filter(user, function (item) {
// 	return item == 'admin';
// });

// console.log(userFilter);
// === fimd ===

let obj = _.find(
	[
		{ id: 1, name: 'Vova' },
		{ id: 2, name: 'Sara' },
	],
	function (item) {
		return item.name === 'bar';
	}
);

obj = _.find(
	[
		{ id: 1, name: 'Vova' },
		{ id: 2, name: 'Sara' },
	],
	{ id: 2 }
);
console.log(obj);
