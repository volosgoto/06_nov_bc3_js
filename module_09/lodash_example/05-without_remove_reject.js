// let result = _.without([1, 2, 3], 1, 3);
// console.log(result);

// ================== remove ============
// Мутабельный! Меняет исходный объект!
// let users = [
// 	{ id: 1, name: 'Vova' },
// 	{ id: 2, name: 'Sara' },
// ];
// _.remove(users, function (item) {
// 	return item.id === 2;
// });

// console.log(users);
// ================== filter ============
// Чтобы получить новый массив нужно использовать метод filter
// let users = [
// 	{ id: 1, name: 'Vova' },
// 	{ id: 2, name: 'Sara' },
// ];
// let filtrUser = _.filter(users, function (item) {
// 	return item.id !== 2;
// });

// console.log(filtrUser);

// ================== reject ============
// Обратный фильтру! Удалит все что написано в условии
let users = [
	{ id: 1, name: 'Vova' },
	{ id: 2, name: 'Sara' },
];
let rejectUser = _.reject(users, function (item) {
	return item.id == 2;
});

console.log(rejectUser);
