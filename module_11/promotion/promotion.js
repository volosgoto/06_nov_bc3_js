// //сделать таймер
// //25.12.2020, 0hours, 0minute, 0sec,
// //1608847200000
// //1608328800000

// const refs = {
//     days: document.querySelector('.days'),
//     hours: document.querySelector('.hours'),
//     minutes: document.querySelector('.minutes'),
//     seconds: document.querySelector('.seconds'),
// }

// const promotion = setInterval(() => {
//     let now = new Date().getTime()
//     let distance = countDown - now;

//     let days = Math.floor(distance / (1000 * 60 * 60 * 24));
//     let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
//     let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
//     let seconds = Math.floor((distance % (1000 * 60)) / 1000);

//     refs.days.textContent = days;
//     refs.hours.textContent = hours;
//     refs.minutes.textContent = minutes;
//     refs.seconds.textContent = seconds;
    
//     if (distance < 0) {
//         document.body.innerHTML = 'Вы профукали свое время'
//         clearInterval(promotion)
//     } 
// }, 1000)

// const countDown = new Date('dec 08, 2020 00:00:00').getTime();

// //=====================================
class Promotion {
    constructor (countDown) {
        this.countDown = new Date(countDown).getTime()
        this.daysRef = document.querySelector('.days')
        this.hoursRef = document.querySelector('.hours')
        this.minutesRef = document.querySelector('.minutes')
        this.secondsRef = document.querySelector('.seconds')
        this.timer = document.querySelector('.timer')
        this.days = ''
        this.hours = ''
        this.minutes = ''
        this.seconds = ''
        this.distance = null;
    }
    promotion() {
        setInterval(() => {
            let distance = this.countDown - this.getCurrentTime()
            this.days = Math.floor(distance / (1000 * 60 * 60 * 24));
            this.hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            this.minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            this.seconds = Math.floor((distance % (1000 * 60)) / 1000);

            this.distance = distance;
            this.displayData()

        }, 1000) 
    }
    getCurrentTime() {
        return new Date().getTime()
    }
    displayData() {
        this.daysRef.textContent = this.days;
        this.hoursRef.textContent = this.hours;
        this.minutesRef.textContent = this.minutes;
        this.secondsRef.textContent = this.seconds;

        if (this.distance < 0) {
            document.body.innerHTML = 'Вы профукали свое время'
            this.clearInterval(promotion())
        } 
    }
    init() {
        return this.promotion()
    }
}
const cristmasProm = new Promotion('dec 10, 2020 00:00:00');
cristmasProm.init()