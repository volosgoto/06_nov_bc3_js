// Function Declaration - объекление ф-ции
// function sayName() {}

// Function Epression - функционильное выражение
// const sayName = function() {}

// Arrow function - стрелочная ф-ция
// const sayName = () => {}
