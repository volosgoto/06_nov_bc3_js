const inputSize = document.querySelector('#font-size-control');
const textSize = document.querySelector('#text');

inputSize.addEventListener('input', onInput);
function onInput() {
    const size = inputSize.value;
    textSize.style.fontSize = size + 'px';
}