// URL -  адресс в сети, это местонахождение ресурса или домена и способ подключения (протокол передачи данных)
// https://www.youtube.com/
// https://hastebin.com/


// ============================================
// URN - имя ресурса в сети, но только определяет название ресурса, не говорит где домен и как к нему подключиться.

// https://www.youtube.com     /watch?v=uIJTCOSiaAU&ab_channel=TVZV
//           URL             +                 URN

// /watch?v=uIJTCOSiaAU&ab_channel=TVZV
// ====================================================

// URI - это имя и адресс ресурса в сети, ключает в себя URL + URN
// https://www.youtube.com/watch?v=uIJTCOSiaAU&ab_channel=TVZV


// ===========================================================
// URI DETAIL
https://pixabay.com/api/?key=афврпвапвапц345235&q=yellow+flowers&image_type=photo
https://www.youtube.com/watch?v=uIJTCOSiaAU&ab_channel=TVZV/#part8

// Query string
?ключ=знчение&ключ=знчение&ключ=знчение/#part8

watch? ------------  начало   Query string
// q?
// search?
// v?

v=uIJTCOSiaAU
&
ab_channel=TVZV
#part8